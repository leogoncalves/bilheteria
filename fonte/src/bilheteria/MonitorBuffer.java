package bilheteria;

public class MonitorBuffer {
	  
	  boolean produzindo;
	  int produzidos;	  
	  int tamanhoDoBuffer;

	  public MonitorBuffer(int tamanhoDoBuffer) {
		  
	    this.produzindo = false;
	    this.produzidos = 0;	    
	    this.tamanhoDoBuffer = tamanhoDoBuffer;
	    
	  }

	  public synchronized void EntraConsumidor() throws InterruptedException {

	    while(this.produzidos == 0 || this.produzindo) {
	    	
	      wait();
	      
	    }
	    
	  }

	  public synchronized void SaiConsumidor() {
		  
		this.produzidos--;
		
	    if(this.produzidos < this.tamanhoDoBuffer ) {
	    	
	    	notifyAll();
	    	
	    }

	  }

	  public synchronized void EntraProdutor() throws InterruptedException {
	    
		while(this.produzindo || this.produzidos == tamanhoDoBuffer) {
	      
			wait();
	    
		}

	    this.produzindo = true;	    

	  }

	  public synchronized void SaiProdutor() {
		  
		  this.produzidos++;	
		  this.produzindo = false;
		  
		  notifyAll();

	  }
}
