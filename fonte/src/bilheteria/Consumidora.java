package bilheteria;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class Consumidora extends Thread {
	
	private String[] buffer;
	private PrintWriter writer;
	private int posicao;
	private int tamanhoBuffer;	
	private String nomeDoArquivo = "";
	private MonitorBuffer monitorBuffer;
	private int cont = 0;

	public Consumidora(String[] buffer, int tamanhoBuffer,
			String nomeDoArquivo, MonitorBuffer monitorBuffer) throws UnsupportedEncodingException {
		
		super();
		this.tamanhoBuffer = tamanhoBuffer;
		this.buffer = buffer;
		this.nomeDoArquivo =  nomeDoArquivo;
		this.posicao = 0;		
		this.monitorBuffer = monitorBuffer;
		
		try {
			this.writer = new PrintWriter(nomeDoArquivo, "UTF-8");
		}
		catch(UnsupportedEncodingException uee) {
			uee.printStackTrace();
		}
		catch(FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void escreverNoArquivo() throws InterruptedException{
		monitorBuffer.EntraConsumidor();
		System.out.println("Consumidor entrou");
		System.out.println(buffer[posicao]);
		this.writer.println(buffer[posicao]);
		cont++;
		posicao = (posicao+1) % tamanhoBuffer;
		monitorBuffer.SaiConsumidor();
		
		System.out.println("Consumidor saiu");
	}

	@Override
	public void run(){
//		System.out.println("Estamos na thread consumidora");
		try {
			while(cont < 12) {
				escreverNoArquivo();
			}			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			writer.close();
		}
	}

}
