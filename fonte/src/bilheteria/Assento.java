package bilheteria;
import java.util.Random;
public class Assento {

  int totalDeAssentos;
  int assentosAlocados;
  Integer[] mapaDeAssentos;

  public Assento(int totalDeAssentos){
    this.assentosAlocados = 0;
    this.totalDeAssentos = totalDeAssentos;
    this.mapaDeAssentos = new Integer[this.totalDeAssentos];
    for(int i = 0; i < this.totalDeAssentos; i++) {
    	this.mapaDeAssentos[i] = 0;
    }
  }

  public String VisualizaAssento() {
	  int codigoOp = 1;
      String str = "[";
	  for(int i = 0; i < totalDeAssentos; i++) {
		  str += mapaDeAssentos[i];
		  if (i != totalDeAssentos - 1) {
			  str += (", "); 
		  }
	  }
	  str += ("]");
	  //System.out.println(str);
    return str;
	  
  }

  public int[] AlocaAssentoLivre(int id) {
      int codigoOp = 2;
      int assento;
      int[] retorno = new int[2];
      retorno[1] = 0;
      
      if (assentosAlocados == totalDeAssentos) {
          retorno[0] = 0;
          return retorno;
      }

      Random aleatorio = new Random();
      assento = aleatorio.nextInt(totalDeAssentos);

      while(mapaDeAssentos[assento] != 0) {

          assento = aleatorio.nextInt(totalDeAssentos);
          //System.out.println("Assento: " + assento);

      }

      this.mapaDeAssentos[assento] = id;
      this.assentosAlocados++;
      retorno[0] = 1;
      retorno[1] = assento;
      return retorno;
  }

  public int AlocaAssentoDado(int id, int assento) {
	  int codigoOp = 3;
      if (mapaDeAssentos[assento] == 0) {
          mapaDeAssentos[assento] = id;

          this.assentosAlocados++;
          return 1;
          
      }

      return 0;
  }

  public int liberaAssento(int id, int assento) {
	  int codigoOp = 4;
      if (mapaDeAssentos[assento] == id) {
          mapaDeAssentos[assento] = 0;
          this.assentosAlocados--;
          return 1;
      }

      return 0;

  }
}
