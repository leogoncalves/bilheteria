package bilheteria;

import java.util.Random;

public class Usuario extends Thread {

	private Monitor monitor;
	private MonitorBuffer monitorBuffer;
	private Assento assento;
	private int id;
	private static int posicao = 0;
	
	public Usuario(Monitor monitor, MonitorBuffer monitorBuffer, Assento assento, int id) {
		super();
		this.monitor = monitor;
		this.monitorBuffer = monitorBuffer;
		this.assento = assento;
		this.id = id;		
	}

	public synchronized void escreverNoBuffer(String dados) throws InterruptedException {
		//System.out.println("Passando pelo método escreverNoBuffer");
		monitorBuffer.EntraProdutor();
		
		System.out.println("saimos do wait de escrever no buffer");
		Main.buffer[posicao] = dados;
		posicao = (posicao + 1) % Main.tamanhoBuffer;		
		monitorBuffer.SaiProdutor();
	}

	@Override
	public void run() {
		int operacao;
		Random aleatorio = new Random();
		String dados = "";
		int aux;
		int paraAlocar;
		int paraLiberar;
		
		for (int i = 0; i < 3; i++) {
			operacao = aleatorio.nextInt(4) + 1;
//			System.out.println("OPERACAO: " + operacao);
			switch (operacao) {
			case 1:
				try {
					monitor.EntraLeitor();
					Thread.sleep(500);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
//				System.out.println("Visualiza Assento - [Thread]: " + this.id);
				assento.VisualizaAssento();
				dados = operacao+", " +id + ", "+ assento.VisualizaAssento();
				try {
					escreverNoBuffer(dados);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				monitor.SaiLeitor();
				break;
			case 2:
				try {
					monitor.EntraEscritor();
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				aux = assento.AlocaAssentoLivre(this.id)[1];
				dados = operacao + ", " + this.id + ", " + aux + ", " + assento.VisualizaAssento();
//				System.out.println("Aloca Assento Livre - [Thread]: " + this.id);
				try {
					escreverNoBuffer(dados);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				monitor.SaiEscritor();
				break;
			case 3:
				try {
					monitor.EntraEscritor();
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				paraAlocar = aleatorio.nextInt(assento.totalDeAssentos);
				assento.AlocaAssentoDado(this.id, paraAlocar);
				dados = operacao+", " +id + ", "+ paraAlocar + ", "+ assento.VisualizaAssento();
				try {
					escreverNoBuffer(dados);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
//				System.out.println("Aloca Assento Dado - [Thread]: " + this.id);
				monitor.SaiEscritor();
				break;
			case 4:
				try {
					monitor.EntraEscritor();
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				paraLiberar = aleatorio.nextInt(assento.totalDeAssentos);
				assento.liberaAssento(this.id, paraLiberar);
				dados = operacao+", " +id + ", "+ paraLiberar + ", "+  assento.VisualizaAssento();
				try {
					escreverNoBuffer(dados);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
//				System.out.println("Libera Assento - [Thread]: " + this.id);
				monitor.SaiEscritor();
				break;
			default:
				continue;
//				try {
//					monitor.EntraLeitor();
//				} catch (InterruptedException e1) {
//					// TODO Auto-generated catch block
//					e1.printStackTrace();
//				}
//				//System.out.println("Visualiza Assento - [Thread]: " + this.id);
//				assento.VisualizaAssento();
//				dados = operacao+", " +id + ", "+ assento.VisualizaAssento();
//				monitor.SaiLeitor();
//				break;
			}
//			try {
//				System.out.println("DADOS INSERIDOS NO BUFFER");
//				escreverNoBuffer(dados);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
		}

		super.run();
	}

}